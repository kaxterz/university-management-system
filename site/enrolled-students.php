<?php
$pageTitle = 'Enrolled Students';
include 'header.php';
?>

<div class="container-fluid main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb default-color">
                    <li class="breadcrumb-item"><a class="white-text" href="index.php">Home</a></li>
                    <li class="breadcrumb-item"><a class="white-text" href="#">Unit</a></li>
                    <li class="breadcrumb-item active">Enrolled Students</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-3rem">
        <div class="col-md-4 pr-0">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Units</strong>
                </h5>
                <div class="card-body pt-0 pl-0 pr-0 pb-0">
                    <ul class="list-group list-group-flush" id="unitListOl"></ul>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Enrolled Students</strong>
                </h5>

                <!--Card content-->
                <div class="card-body">
                    <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Student Name</th>
                                <th>Student ID</th>
                                <th>Email</th>
                                <th>Enrolled Date</th>
                            </tr>
                        </thead>
                        <tbody id="tbody"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include 'footer.php'; ?>

<script>
    $(function() {
        get_all_units();
    });

    function get_all_units() {
        axios.get('http://uodw.test/backend/handler.php?f=get_all_units_for_enrolled_students')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitListOl').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function enrolled_students_list(id) {
        axios.post('http://uodw.test/backend/handler.php?f=enrolled_students_list', {
                id: id
            })
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#tbody').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }
</script>
</body>

</html>