<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <title>University of DoWell - Login</title>
</head>

<body>
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-md-8 mt-4 mb-4">
                <!-- Material form login -->
                <div class="card">

                    <h5 class="card-header default-color white-text text-center py-4">
                        <strong>Login</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">

                        <!-- Form -->
                        <form class="text-center" style="color: #757575;" action="#!" id="form">

                            <!-- Email -->
                            <div class="md-form">
                                <input type="email" id="email" class="form-control">
                                <label for="materialLoginFormEmail">E-mail</label>
                            </div>

                            <!-- Password -->
                            <div class="md-form">
                                <input type="password" id="password" class="form-control">
                                <label for="materialLoginFormPassword">Password</label>
                            </div>

                            <input type="hidden" name="type" id="type">

                            <div class="d-flex justify-content-around">
                                <div>
                                    <!-- Remember me -->
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                                        <label class="form-check-label" for="materialLoginFormRemember">Remember
                                            me</label>
                                    </div>
                                </div>
                                <div>
                                    <!-- Forgot password -->
                                    <a href="">Forgot password?</a>
                                </div>
                            </div>

                            <!-- Sign in button -->
                            <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0"
                                type="submit">Sign in</button>

                            <!-- Register -->
                            <p>Not a member?
                                <a href="register.html">Register</a>
                            </p>

                        </form>
                        <!-- Form -->

                    </div>

                </div>
                <!-- Material form login -->

                 <!--Modal: modalCookie-->
                    <div class="modal fade top" id="modalCookie1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                    aria-hidden="true" data-backdrop="true">
                    <div class="modal-dialog modal-frame modal-top modal-notify modal-info" role="document">
                        <!--Content-->
                        <div class="modal-content">
                            <!--Body-->
                            <div class="modal-body">
                                <div class="row d-flex justify-content-center align-items-center">

                                    <p class="pt-3 pr-2">Who are you ?</p>

                                    <a type="button" class="btn btn-primary" id="student" data-dismiss="modal">Student</a>
                                    <a type="button" class="btn btn-default" id="staff" data-dismiss="modal">Staff</a>
                                </div>
                            </div>
                        </div>
                        <!--/.Content-->
                    </div>
                </div>
                <!--Modal: modalCookie-->

            </div>
        </div>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/js/mdb.min.js"></script>
    <!-- Axios for AJAX calls -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- Toastr for notifications -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        type = ''
        $(function () {

            $('#student').click(function(){
                $('#type').val('student')
            });

            $('#staff').click(function(){
                $('#type').val('staff')
            });

            $('#modalCookie1').modal({
                backdrop: 'static',
                keyboard: false
            })

            $('#form').submit(function (e) {
                
                e.preventDefault();
                var type = $('#type').val();
                var email = $('#email').val();
                var password = $('#password').val();

                if (password.length < 1) {
                    alert('Password is required');
                }
                
                if (email.length < 1) {
                    alert('Email is required');
                } else {
                    var regEx = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
                    var validEmail = regEx.test(email);
                    if (!validEmail) {
                        alert('Enter a valid email address');
                    }
                }

                axios.post('http://uodw.test/backend/handler.php?f=login', {
                    type: type,
                    email: email,
                    password: password
                })
                .then(function (response) {
                    if(response.data.status_code == 200){
                        window.location.replace('index.php')
                    }else{
                        toastr.error(response.data.message)
                    }
                })
                .catch(function (error) {
                    toastr.error(error)
                });
            });
        });
    </script>
</body>

</html>