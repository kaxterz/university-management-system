<?php 
    session_start();
    $activePage = basename($_SERVER['PHP_SELF'], ".php");
    if (!isset($_SESSION["user_id"])) {
        header('Location: http://uodw.test/login.php');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/css/mdb.min.css" rel="stylesheet">
    <!-- MDBootstrap Datatables  -->
    <link href="assets/css/datatables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <title>University of DoWell - <?php echo $pageTitle; ?></title>
</head>

<body>
    <!--Navbar -->
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark fixed-top default-color">
        <a class="navbar-brand" href="#"><i class="far fa-building"></i></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333" aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= ($activePage == 'index') ? 'active':''; ?>">
                    <a class="nav-link" href="index.php">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item <?= ($activePage == 'academic-staff') ? 'active':''; ?>">
                    <a class="nav-link" href="academic-staff.php">Academic</a>
                </li>
                <li class="nav-item <?= ($activePage == 'tutorial-allocation') ? 'active':''; ?>">
                    <a class="nav-link" href="tutorial-allocation.php">Tutorial Allocation</a>
                </li>
                <?php if ($_SESSION["user_type"] == 'student') { ?>
                    <li class="nav-item <?= ($activePage == 'timetable') ? 'active':''; ?>">
                        <a class="nav-link" href="timetable.php">Individual Time Table</a>
                    </li>
                <?php } ?>
                <li class="nav-item dropdown <?= ($activePage == 'unit-management') || ($activePage == 'unit-details') ||
                    ($activePage == 'enrolled-students') || ($activePage == 'unit-enrolment') ? 'active':''; ?>">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Unit
                    </a>
                    <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
                        <?php if ($_SESSION["user_type"] == 'staff') { ?>
                            <a class="dropdown-item" href="unit-management.php">Unit Management</a>
                            <a class="dropdown-item" href="unit-details.php">Unit Details</a>
                            <a class="dropdown-item" href="enrolled-students.php">Enrolled Students</a>
                        <?php } else { ?>
                            <a class="dropdown-item" href="unit-enrolment.php">Unit Enrolment</a>
                        <?php } ?>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto nav-flex-icons">
                <?php if (isset($_SESSION["user_id"])) { ?>
                    <li class="nav-item dropdown <?= ($activePage == 'my-account') ? 'active':''; ?>">
                        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user"></i> <?php echo $_SESSION['name']; ?> </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-info" aria-labelledby="navbarDropdownMenuLink-4">
                            <a class="dropdown-item" href="my-account.php">My account</a>
                            <a type="button" id="logout" class="dropdown-item">Log out</a>
                        </div>
                    </li>
                <?php } else { ?>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="register.php">
                            <i class="fas fa-user-plus"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link waves-effect waves-light" href="login.php">
                            <i class="fas fa-sign-in-alt"></i>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
    <!--/.Navbar -->