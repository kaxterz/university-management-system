<?php
$pageTitle = 'My Account';
include 'header.php';
?>

<div class="container-fluid main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb default-color">
                    <li class="breadcrumb-item"><a class="white-text" href="index.php">Home</a></li>
                    <li class="breadcrumb-item active">My Account</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-3rem">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Profile</strong>
                </h5>

                <!--Card content-->
                <div class="card-body pt-0" id="profile">
                    <form class="text-center" action="#!" id="updateAccInfo"></form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>

<script>
    $(function() {
        load_account_data()
    })

    function load_account_data() {
        axios.get('http://uodw.test/backend/handler.php?f=load_account_data')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#updateAccInfo').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    $('#updateAccInfo').submit(function(e) {
        console.log('brp');

        e.preventDefault();
        var name = $('#name').val();
        var email = $('#email').val();
        var address = $('#address').val();
        var phone = $('#phone').val();
        var password = $('#password').val();
        var cpassword = $('#cpassword').val();
        var user_type = $('#user_type').val();

        if (user_type == 'student') {
            var dob = $('#dob').val();
        } else {
            var qualifications = $('#qualifications').val();
            var expertise = $('#expertise').val();
        }

        if(password != ''){
            var regEx = /^.*(?=.{6,12})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/;
            var validPassword = regEx.test(password);
            if (!validPassword) {
                toastr.error('Password should contain at least 6 characters to 12 characters with minimum 1 lowecase and minimum 1 uppercase with minimum 1 digit and 1 special character');
                return false;
            }
            if (cpassword.length < 1) {
                toastr.error('Confirm password is required');
                return false;
            } else if (cpassword != password) {
                toastr.error("Confirm password doesn\'t match");
                return false;
            }
        }

        var url  = '';
        var d = '';

        if(user_type == 'student'){
            url = 'http://uodw.test/backend/handler.php?f=edit_student_profile'
            d = {
                name: name,
                email: email,
                address: address,
                phone: phone,
                dob:dob,
                password: password
            }
        }else{
            url = 'http://uodw.test/backend/handler.php?f=edit_staff_profile'
            d = {
                name: name,
                email: email,
                address: address,
                phone: phone,
                expertise:expertise,
                qualifications:qualifications,
                password: password
            }
        }

        axios.post(url,d)
            .then(function(response) {
                console.log('ress', response);

                if (response.data.status_code == 200) {
                    toastr.success(response.data.message)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    });
</script>

</body>

</html>