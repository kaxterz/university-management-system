<?php
$pageTitle = 'Unit Details';
include 'header.php';
?>

<div class="container-fluid main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb default-color">
                    <li class="breadcrumb-item"><a class="white-text" href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a class="white-text" href="unit-details.html">Unit</a></li>
                    <li class="breadcrumb-item active">Unit Details</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-3rem">
        <div class="col-md-4 pr-0">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Unit List</strong>
                </h5>

                <!--Card content-->
                <div class="card-body pt-0 pl-0 pr-0">
                    <ul class="list-group list-group-flush" id="unitListOl"></ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Unit Details</strong>
                </h5>

                <!--Card content-->
                <div class="card-body pt-0" id="unitDetailsBody"></div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>

<script>
    $(function() {
        list_units()
    })

    function list_units() {
        axios.get('http://uodw.test/backend/handler.php?f=list_units')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitListOl').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function list_unit_details(id) {
        axios.post('http://uodw.test/backend/handler.php?f=list_unit_details', {
                id: id
            })
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitDetailsBody').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }
</script>

</body>

</html>