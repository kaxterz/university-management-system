<footer class="page-footer font-small teal fixed-bottom mt-4">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2020 Copyright:
        <a href="https://mdbootstrap.com/"> UODW.com</a>
    </div>
    <!-- Copyright -->

</footer>

<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/js/mdb.min.js"></script>
<!-- Axios for AJAX calls -->
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<!-- Toastr for notifications -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script type="text/javascript" src="assets/js/datatables.min.js"></script>

<script>
    $('#logout').click(function(){
        axios.post('http://uodw.test/backend/handler.php?f=logout')
        .then(function (response) {

            if(response.data.status_code == 200){
                window.location.replace('index.php')
            }else{
                toastr.error(response.data.message)
            }
        })
        .catch(function (error) {
            toastr.error(error)
        });
    });
</script>