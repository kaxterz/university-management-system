<?php 
    $pageTitle = 'Unit Management';
    include 'header.php'; 
?>

<div class="container-fluid main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb default-color">
                    <li class="breadcrumb-item"><a class="white-text" href="index.php">Home</a></li>
                    <li class="breadcrumb-item"><a class="white-text" href="#">Unit</a></li>
                    <li class="breadcrumb-item active">Unit Management</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-3rem">
        <div class="col-md-4 pr-0">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Units</strong>
                    <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#addNewUnitModel"><i class="fas fa-plus text-white"></i>&nbsp;New Unit</button>
                </h5>
                <div class="card-body pt-0 pl-0 pr-0 pb-0">
                    <ul class="list-group list-group-flush" id="unitListOl"></ul>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Edit Unit</strong>
                </h5>

                <!--Card content-->
                <div class="card-body pt-0 pl-0 pr-0">
                    <form class="text-center mt-4 mr-4" action="#!" id="unitDetailsForm"></form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Central Modal Small -->
<div class="modal fade" id="addNewUnitModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-md" role="document">


        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="addNewUnitModel">Create New Unit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="text-center" action="#!" id="newUnitform">
                <div class="modal-body">
                    <div class="md-form">
                        <input type="text" id="unitName" class="form-control" required>
                        <label for="form1">Unit Name</label>
                    </div>
                    <div class="md-form">
                        <textarea class="md-textarea form-control" id="unitDesc" rows="3" required></textarea>
                        <label for="form7">Description</label>
                    </div>
                    <select class="browser-default custom-select mt-4 mb-4" id="unitCampus" required>
                        <option selected disabled>Select Campus</option>
                        <option value="1">Pandora</option>
                        <option value="2">Rivendell</option>
                        <option value="3">Neverland</option>
                    </select>
                    <select class="browser-default custom-select" id="unitSem" required>
                        <option selected disabled>Select Semester</option>
                        <option value="1">Semester 01</option>
                        <option value="2">Semester 02</option>
                        <option value="3">Winter School</option>
                        <option value="4">Spring School</option>
                    </select>
                    <select class="browser-default custom-select mt-4 mb-4" id="unitCoordinator" required></select>
                    <select class="browser-default custom-select mb-4" id="unitLecturer" required></select>
                    <select class="browser-default custom-select mb-4" id="unitTutor" required></select>
                    <select class="browser-default custom-select mb-4" id="unitLecDay" required>
                        <option selected disabled>Select Lecture Day</option>
                        <option value="1">Monday</option>
                        <option value="2">Tuesday</option>
                        <option value="3">Wednesday</option>
                        <option value="4">Thursday</option>
                        <option value="5">Friday</option>
                    </select>
                    <div class="md-form">
                        <input type="time" id="unitLecTime" class="form-control" required>
                        <label for="form1">Lecture Time</label>
                    </div>
                    <select class="browser-default custom-select mb-4" id="unitTuteDay" required>
                        <option selected disabled>Select Tutorial Day</option>
                        <option value="1">Monday</option>
                        <option value="2">Tuesday</option>
                        <option value="3">Wednesday</option>
                        <option value="4">Thursday</option>
                        <option value="5">Friday</option>
                    </select>
                    <div class="md-form">
                        <input type="time" id="unitTuteTime" class="form-control" required>
                        <label for="form1">Tutorial Time</label>
                    </div>
                    <div class="md-form">
                        <input type="text" pattern="[a-zA-Z0-9\s]+" title="Only letters,numbers and spaces are allowed" id="unitLocation" class="form-control" required>
                        <label for="form1">Location</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default btn-sm">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Central Modal Small -->

<?php include 'footer.php'; ?>

<script>
    $(function() {
        get_all_units();
        load_tutors_uc_lec();
        $('#newUnitform').submit(function(e) {

            e.preventDefault();
            var unitName = $('#unitName').val();
            var unitDesc = $('#unitDesc').val();
            var unitSem = $('#unitSem').val();
            var unitCampus = $('#unitCampus').val();
            var unitCoordinator = $('#unitCoordinator').val();
            var unitLecturer = $('#unitLecturer').val();
            var unitTutor = $('#unitTutor').val();
            var unitLecDay = $('#unitLecDay').val();
            var unitLecTime = $('#unitLecTime').val();
            var unitTuteDay = $('#unitTuteDay').val();
            var unitTuteTime = $('#unitTuteTime').val();
            var unitLocation = $('#unitLocation').val();

            if (unitName.length < 1) {
                alert('Unit name is required');
                return false
            }

            if (unitDesc.length < 1) {
                alert('Unit description is required');
                return false
            }

            if (unitSem.length < 1) {
                alert('Semester is required');
                return false
            }

            if (unitCampus.length < 1) {
                alert('Campus is required');
                return false
            }


            axios.post('http://uodw.test/backend/handler.php?f=add_new_unit', {
                    unitName: unitName,
                    unitDesc: unitDesc,
                    unitSem: unitSem,
                    unitCampus: unitCampus,
                    unitCoordinator: unitCoordinator,
                    unitLecturer: unitLecturer,
                    unitTutor: unitTutor,
                    unitLecDay: unitLecDay,
                    unitLecTime: unitLecTime,
                    unitTuteDay: unitTuteDay,
                    unitTuteTime: unitTuteTime,
                    unitLocation: unitLocation
                })
                .then(function(response) {
                    if (response.data.status_code == 200) {
                        toastr.success(response.data.message)
                        get_all_units()
                    } else {
                        toastr.error(response.data.message)
                    }
                })
                .catch(function(error) {
                    toastr.error(error)
                });
        });

    });

    function load_tutors_uc_lec() {
        axios.get('http://uodw.test/backend/handler.php?f=load_tutors_uc_lec')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitCoordinator').html(response.data.unit_coordinator_res)
                    $('#unitLecturer').html(response.data.unit_lecturer_res)
                    $('#unitTutor').html(response.data.unit_tutor_res)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function get_all_units() {
        axios.get('http://uodw.test/backend/handler.php?f=get_all_units')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitListOl').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function load_unit_details(id) {
        axios.post('http://uodw.test/backend/handler.php?f=load_unit_details', {
                id: id
            })
            .then(function(response) {
                console.log('res',response);
                if (response.data.status_code == 200) {
                    
                    
                    $('#unitDetailsForm').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    $('#unitDetailsForm').submit(function(e) {
        console.log('brp');

        e.preventDefault();
        var unitId = $('#editUnitId').val();
        var unitName = $('#editUnitName').val();
        var unitDesc = $('#editUnitDesc').val();
        var unitSem = $('#editUnitSem').val();
        var unitCampus = $('#editUnitCampus').val();
        var unitLecDay = $('#editLecDay').val();
        var unitLecTime = $('#editLecTime').val();
        var unitTuteDay = $('#editTuteDay').val();
        var unitTuteTime = $('#editTuteTime').val();
        var unitLocation = $('#editLocation').val();

        axios.post('http://uodw.test/backend/handler.php?f=edit_unit', {
                id: unitId,
                unitName: unitName,
                unitDesc: unitDesc,
                unitSem: unitSem,
                unitCampus: unitCampus,
                unitLecDay: unitLecDay,
                unitLecTime: unitLecTime,
                unitTuteDay: unitTuteDay,
                unitTuteTime: unitTuteTime,
                unitLocation: unitLocation
            })
            .then(function(response) {
                console.log('ress', response);

                if (response.data.status_code == 200) {
                    toastr.success(response.data.message)
                    // $('#unitDetailsBody').html()
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    });

    function remove_unit(id) {
        $.confirm({
            title: 'Continue ?',
            content: 'Do you want to remove this Unit ?',
            type: 'blue',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Yes',
                    btnClass: 'btn-info',
                    action: function() {
                        axios.post('http://uodw.test/backend/handler.php?f=remove_unit', {
                                id: id
                            })
                            .then(function(response) {
                                if (response.data.status_code == 200) {
                                    toastr.success(response.data.message)
                                    get_all_units()
                                } else {
                                    toastr.error(response.data.message)
                                }
                            })
                            .catch(function(error) {
                                toastr.error(error)
                            });
                    }
                },
                close: function() {}
            }
        });

    }
</script>
</body>

</html>