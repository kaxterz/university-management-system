<?php
$pageTitle = 'Academic Staff';
include 'header.php';
?>


<div class="container-fluid main-wrapper">
    <div class="row">
        <div class="col-md-12">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb default-color">
                    <li class="breadcrumb-item"><a class="white-text" href="index.html">Home</a></li>
                    <li class="breadcrumb-item"><a class="white-text" href="#">Academic</a></li>
                    <li class="breadcrumb-item active">Academic Staff</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="row mb-3rem">
        <div class="col-md-4 pr-0">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Academic Staff</strong>
                </h5>
                <div class="card-body pt-0 pl-0 pr-0 pb-0">
                    <!--Accordion wrapper-->
                    <div class="accordion md-accordion accordion-5" id="accordionEx5" role="tablist" aria-multiselectable="true">
                        <div id="staffDiv"></div>
                    </div>
                </div>
                <!--/.Accordion wrapper-->
            </div>
        </div>

        <div class="col-md-8">
            <div class="card">
                <h5 class="card-header default-color white-text text-center py-4">
                    <strong>Available Units</strong>
                </h5>

                <!--Card content-->
                <div class="card-body pt-0 pl-0 pr-0">
                    <ul class="list-group list-group-flush" id="unitList"></ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Central Modal Small -->
<div class="modal fade" id="centralModalSm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- Change class .modal-sm to change the size of the modal -->
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Allocate Lecturer for Selected Unit</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="text-center" action="#!" id="updateUnitStaffForm">
                <div class="modal-body">
                    <div id="listCoordinators" class="mb-4"></div>
                    <div id="listLecs" class="mb-4"></div>
                    <div id="listTutors"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-default btn-sm">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Central Modal Small -->

<?php include 'footer.php'; ?>

<script>
    selected_unit_id = 0;
    $(function() {
        load_academic_staff();
        get_all_units_for_academic();
        list_tutors();
        list_lecturers();
        list_coordinators();

        $('#updateUnitStaffForm').submit(function(e) {
            e.preventDefault();
            update_unit_staff_from_academic();
        });
    });

    function model_data(id) {
        selected_unit_id = id
    }

    function remove_staff(id) {
        $.confirm({
            title: 'Continue ?',
            content: 'Do you want to remove this person ?',
            type: 'blue',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Yes',
                    btnClass: 'btn-info',
                    action: function() {
                        axios.post('http://uodw.test/backend/handler.php?f=remove_staff', {
                                id: id
                            })
                            .then(function(response) {
                                if (response.data.status_code == 200) {
                                    toastr.success(response.data.message)
                                    load_academic_staff()
                                } else {
                                    toastr.error(response.data.message)
                                }
                            })
                            .catch(function(error) {
                                toastr.error(error)
                            });
                    }
                },
                close: function() {}
            }
        });
    }

    function update_unit_staff_from_academic() {
        axios.post('http://uodw.test/backend/handler.php?f=update_unit_staff_from_academic', {
                selected_unit_id: selected_unit_id,
                unit_co: $('#editUnitCo').val(),
                unit_lec: $('#editUnitLecturer').val(),
                unit_tutor: $('#editUnitTutor').val()
            })
            .then(function(response) {
                if (response.data.status_code == 200) {
                    toastr.success(response.data.message)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function load_academic_staff() {
        axios.get('http://uodw.test/backend/handler.php?f=load_academic_staff')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#staffDiv').html(response.data.staff_res)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                console.log(error);

                toastr.error(error)
            });
    }

    function get_all_units_for_academic() {
        axios.get('http://uodw.test/backend/handler.php?f=get_all_units_for_academic')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#unitList').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function list_coordinators() {
        axios.get('http://uodw.test/backend/handler.php?f=list_coordinators')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#listCoordinators').html(response.data.unit_coordinator_res)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function list_lecturers() {
        axios.get('http://uodw.test/backend/handler.php?f=list_lecturers')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#listLecs').html(response.data.unit_lecturer_res)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }

    function list_tutors() {
        axios.get('http://uodw.test/backend/handler.php?f=list_tutors')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#listTutors').html(response.data.unit_tutor_res)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
    }
</script>
</body>

</html>