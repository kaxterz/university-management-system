<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    <title>University of DoWell - Student Register</title>
</head>

<body>
    <div class="container">
        <div class="row d-flex flex-row justify-content-center">
            <div class="col-md-8 mt-4 mb-4">
                <!-- Material form register -->
                <div class="card">

                    <h5 class="card-header default-color white-text text-center py-4">
                        <strong>Register</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0">

                        <!-- Form -->
                        <form class="text-center" style="color: #757575;" action="#!" id="form">
                            

                            <div class="form-row">
                                <div class="col">
                                    <!-- First name -->
                                    <div class="md-form">
                                        <input type="text" id="studentId" class="form-control">
                                        <label for="materialRegisterFormFirstName">Student ID</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <!-- Last name -->
                                    <div class="md-form">
                                        <input type="text" id="name" class="form-control">
                                        <label for="materialRegisterFormLastName">Name</label>
                                    </div>
                                </div>
                            </div>

                            <!-- E-mail -->
                            <div class="md-form mt-0">
                                <input type="email" id="email" class="form-control">
                                <label for="materialRegisterFormEmail">E-mail</label>
                            </div>

                            <!-- Password -->
                            <div class="md-form">
                                <input type="password" id="password" class="form-control"
                                    aria-describedby="materialRegisterFormPasswordHelpBlock">
                                <label for="materialRegisterFormPassword">Password</label>
                                <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                                    At least 6 to 12 characters, 1 lowecase, 1 uppercase, 1 digit and 1 special character
                                </small>
                            </div>

                            <!-- Confirm Password -->
                            <div class="md-form">
                                <input type="password" id="cpassword" class="form-control"
                                    aria-describedby="materialRegisterFormPasswordHelpBlock">
                                <label for="materialRegisterFormPassword">Confirm Password</label>
                            </div>

                            <!-- Address -->
                            <div class="md-form">
                                <input type="text" id="address" class="form-control"
                                    aria-describedby="materialRegisterFormPasswordHelpBlock">
                                <label for="materialRegisterFormPassword">Address</label>
                                <small id="materialRegisterFormPasswordHelpBlock" class="form-text text-muted mb-4">
                                    Optional
                                </small>
                            </div>

                            <div class="form-row">
                                <div class="col">
                                    <!-- DOB -->
                                    <div class="md-form">
                                        <input type="date" id="dob" class="form-control">
                                        <label for="materialRegisterFormFirstName">Date of Birth</label>
                                        <small id="materialRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
                                            Optional
                                        </small>
                                    </div>
                                </div>

                                <div class="col">
                                    <!-- Phone number -->
                                    <div class="md-form">
                                        <input type="text" id="phone" class="form-control"
                                            aria-describedby="materialRegisterFormPhoneHelpBlock">
                                        <label for="materialRegisterFormPhone">Phone number</label>
                                        <small id="materialRegisterFormPhoneHelpBlock" class="form-text text-muted mb-4">
                                            Optional
                                        </small>
                                    </div>
                                </div>
                            </div>

                            <!-- Sign up button -->
                            <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0"
                                type="submit">Submit</button>

                        </form>
                        <!-- Form -->

                    </div>

                </div>
                <!-- Material form register -->
            </div>
            <!--Modal: modalCookie-->
            <div class="modal fade top" id="modalCookie1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                aria-hidden="true" data-backdrop="true">
                <div class="modal-dialog modal-frame modal-top modal-notify modal-info" role="document">
                    <!--Content-->
                    <div class="modal-content">
                        <!--Body-->
                        <div class="modal-body">
                            <div class="row d-flex justify-content-center align-items-center">

                                <p class="pt-3 pr-2">Who are you ?</p>

                                <a type="button" class="btn btn-primary" data-dismiss="modal">Student</a>
                                <a href="lec-register.php" type="button" class="btn btn-default" >Staff</a>
                            </div>
                        </div>
                    </div>
                    <!--/.Content-->
                </div>
            </div>
            <!--Modal: modalCookie-->
        </div>
    </div>

    <!-- JQuery -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.15.0/js/mdb.min.js"></script>
     <!-- Axios for AJAX calls -->
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <!-- Toastr for notifications -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script>
        $(function () {
            $('#modalCookie1').modal({
                backdrop: 'static',
                keyboard: false
            })
            $('#form').submit(function (e) {
                e.preventDefault();
                var name = $('#name').val();
                var studentId = $('#studentId').val();
                var email = $('#email').val();
                var password = $('#password').val();
                var cpassword = $('#cpassword').val();
                var phone = $('#phone').val();
                var address = $('#address').val();
                var dob = $('#dob').val();

                if (name.length < 1) {
                    alert('Name is required');
                }

                if (studentId.length < 1) {
                    alert('Student ID is required');
                }

                if (password.length < 1) {
                    alert('Password is required');
                } else {
                    var regEx = /^.*(?=.{6,12})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[\W_]).*$/;
                    var validPassword = regEx.test(password);
                    if (!validPassword) {
                        alert('Password should contain at least 6 characters to 12 characters with minimum 1 lowecase and minimum 1 uppercase with minimum 1 digit and 1 special character');
                    }
                }

                if (cpassword.length < 1) {
                    alert('Confirm password is required');
                } else if (cpassword != password) {
                    alert("Confirm password doesn\'t match");
                }

                if (email.length < 1) {
                    alert('Email is required');
                } else {
                    var regEx = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
                    var validEmail = regEx.test(email);
                    if (!validEmail) {
                        alert('Enter a valid email address');
                    }
                }

                axios.post('http://uodw.test/backend/handler.php?f=student_registration', {
                    student_id: studentId,
                    name: name,
                    email: email,
                    password: password,
                    phone: phone,
                    address: address,
                    dob: dob
                })
                .then(function (response) {
                    
                    if(response.data.status == 200){
                        toastr.success('Your account have been created successfully !')
                    }else{
                        toastr.error('An error occured.. Try again later !')
                    }
                })
                .catch(function (error) {
                    toastr.error(error)
                });
            });
        });
    </script>
</body>

</html>