<?php 
    $pageTitle = 'Individual Time Table';
    include 'header.php'; 
?>


    <div class="container-fluid main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb default-color">
                        <li class="breadcrumb-item"><a class="white-text" href="index.html">Home</a></li>
                        <li class="breadcrumb-item active"><a href="timetable.html">Individual Time Table</a></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row mb-3rem">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header default-color white-text text-center py-4">
                        <strong>My Timetable</strong>
                    </h5>

                    <!--Card content-->
                    <div class="card-body pt-0 pt-1">
                        <table id="dt-basic-checkbox" class="table table-striped table-bordered" cellspacing="0"
                            width="100%">
                            <thead>
                                <tr>
                                    <th>Unit Name</th>
                                    <th class="th-sm">Lecture Day
                                    </th>
                                    <th class="th-sm">Lecture Time
                                    </th>
                                    <th class="th-sm">Tutorial Day
                                    </th>
                                    <th class="th-sm">Tutorial Time
                                    </th>
                                    <th class="th-sm">Location
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        
    </div>

    <?php include 'footer.php'; ?>

    <script>
        $(function () {
            load_time_table();
        });

        function load_time_table(){
            axios.get('http://uodw.test/backend/handler.php?f=load_time_table')
            .then(function(response) {
                if (response.data.status_code == 200) {
                    $('#tbody').html(response.data.results)
                } else {
                    toastr.error(response.data.message)
                }
            })
            .catch(function(error) {
                toastr.error(error)
            });
        }
    </script>

</body>

</html>