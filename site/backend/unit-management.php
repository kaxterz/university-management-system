<?php 

class UnitManagement {
    


    function add_new_unit(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unitName = trim($data['unitName']);
        $unitDescription = trim(filter_var($data['unitDesc'],FILTER_SANITIZE_STRING));
        $unitCampus = trim($data['unitCampus']);
        $unitSem = trim($data['unitSem']);
        $userId = $_SESSION["user_id"];

        $unitCoordinator = trim($data['unitCoordinator']);
        $unitLecturer = trim($data['unitLecturer']);
        $unitTutor = trim($data['unitTutor']);

        $unitLecDay = trim($data['unitLecDay']);
        $unitLecTime = trim($data['unitLecTime']);
        $unitTuteDay = trim($data['unitTuteDay']);
        $unitTuteTime = trim($data['unitTuteTime']);
        $unitLocation = trim($data['unitLocation']);

        $sql = "INSERT INTO units (unit_name,description,semester,campus,created_by) VALUES 
       ('$unitName', '$unitDescription', '$unitCampus', '$unitSem', '$userId')";

        $res = $conn->exec($sql);
        $last_id = $conn->lastInsertId();

        if($last_id){
            $sql_unit_data = "INSERT INTO unit_data (unit_id,lecturer_id,unit_coordinator_id,tutor_id,lecture_day,lecture_time,tutorial_day,tutorial_time,location,created_by) VALUES 
            ('$last_id', '$unitLecturer', '$unitCoordinator', '$unitTutor', '$unitLecDay', '$unitLecTime', '$unitTuteDay', '$unitTuteTime', '$unitLocation', '$userId')";

            $res = $conn->exec($sql_unit_data);
        }
        
        if($res > 0){
            echo json_encode(['status_code'=>200,'message'=>'Success !']);
        }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
        }
     }


     function get_all_units(){
        $conn = db_conn();
        $stmt = $conn->prepare("SELECT id,unit_name FROM units");
        $stmt->execute(); 
        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {
            $res .= '<li class="list-group-item"><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'&nbsp;&nbsp;
                        <a class="btn-floating float-right btn-sm btn-danger" onclick=remove_unit('.$unit['id'].')><i class="fas fa-trash-alt text-white"></i></a>
                        <a class="btn-floating float-right btn-sm btn-info mr-2" onclick=load_unit_details('.$unit['id'].')><i class="fas fa-edit text-white"></i></a>
                    </li>';
        }

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function list_units(){
        $conn = db_conn();
        $stmt = $conn->prepare("SELECT id,unit_name FROM units");
        $stmt->execute(); 
        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {
            $res .= '<li class="list-group-item"><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'&nbsp;&nbsp;
                        <a class="btn-floating float-right btn-sm btn-info mr-2" onclick=list_unit_details('.$unit['id'].')><i class="fas fa-eye text-white"></i></a>
                    </li>';
        }

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function load_tutors_uc_lec(){
        $conn = db_conn();

        $stmt_uc = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_uc->execute([2]); 

        $unit_coordinator = $stmt_uc->fetchAll();
        $unit_coordinator_res = '<option selected disabled>Select Unit Coordinator</option>';
        foreach ($unit_coordinator as $uc) {
            $unit_coordinator_res .= '  
                        <option value="'.$uc['id'].'">'.$uc['name'].'</option>
            ';
        }

        $stmt_lec = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_lec->execute([3]); 

        $unit_lecturer = $stmt_lec->fetchAll();
        $unit_lecturer_res = '<option selected disabled>Select Unit Lecturer</option>';
        foreach ($unit_lecturer as $ul) {
            $unit_lecturer_res .= '  
                        <option value="'.$ul['id'].'">'.$ul['name'].'</option>
            ';
        }

        $stmt_ut = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_ut->execute([4]); 

        $unit_tutor = $stmt_ut->fetchAll();
        $unit_tutor_res = '<option selected disabled>Select Unit Tutor</option>';
        foreach ($unit_tutor as $ut) {
            $unit_tutor_res .= '  
                        <option value="'.$ut['id'].'">'.$ut['name'].'</option>
            ';
        }

        if($unit_coordinator_res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','unit_coordinator_res'=>$unit_coordinator_res,'unit_tutor_res'=>$unit_tutor_res,'unit_lecturer_res'=>$unit_lecturer_res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function list_unit_details(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['id']);

        $stmt = $conn->prepare("SELECT semesters.semester AS sem,campus.campus AS cam,ts.name AS tutor_name,ucs.name AS uc_name,ls.name AS lec_name,units.* FROM units 
                                INNER JOIN campus ON campus.id = units.campus 
                                INNER JOIN semesters ON semesters.id = units.semester
                                INNER JOIN unit_data ON unit_data.unit_id = units.id
                                LEFT JOIN staff AS ls ON ls.id = unit_data.lecturer_id
                                LEFT JOIN staff AS ucs ON ucs.id = unit_data.unit_coordinator_id
                                LEFT JOIN staff AS ts ON ts.id = unit_data.tutor_id
                                WHERE units.id=?"
                              );
        $stmt->execute([$unit_id]); 

        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {
            $res .= '
                    <div class="pl-0 pt-3">
                        <h4><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'</h4>
                        <ul class="mt-2 li-style">
                            <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Unit Coordinator : '.$unit['uc_name'].'</li>
                            <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Lecturer : '.$unit['lec_name'].'</li>
                            <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Tutor : '.$unit['tutor_name'].'</li>
                        </ul>
                        <h4><i class="fas fa-file-signature"></i>&nbsp;&nbsp;Unit Outline</h4>
                        <p>'.$unit['description'].'</p>
                        <h4><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;Available Campus</h4>
                        <p>'.$unit['campus'].'</p>
                        <h4><i class="fas fa-bookmark"></i>&nbsp;&nbsp;Semester</h4>
                        <p>'.$unit['semester'].'</p>
                    </div>
            ';
        }

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function ue_list_unit_details(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);
        $userId = $_SESSION["user_id"];
        $unit_id = trim($data['id']);

        $stmt = $conn->prepare("SELECT semesters.semester AS sem,campus.campus AS cam,ts.name AS tutor_name,ucs.name AS uc_name,ls.name AS lec_name,units.* FROM units 
                                INNER JOIN campus ON campus.id = units.campus 
                                INNER JOIN semesters ON semesters.id = units.semester
                                INNER JOIN unit_data ON unit_data.unit_id = units.id
                                LEFT JOIN staff AS ls ON ls.id = unit_data.lecturer_id
                                LEFT JOIN staff AS ucs ON ucs.id = unit_data.unit_coordinator_id
                                LEFT JOIN staff AS ts ON ts.id = unit_data.tutor_id
                                WHERE units.id=?"
                              );
        $stmt->execute([$unit_id]); 

        $units = $stmt->fetchAll();
        $res = '';


        $stmt = $conn->prepare('SELECT count(*) AS row_count FROM student_have_units WHERE student_id=? AND unit_id=?');
        $stmt->execute([$userId,$unit_id]); 
        $row = $stmt->fetch();

        if($row['row_count'] == '0'){
            foreach ($units as $unit) {
                $res .= '
                        <div class="pl-0 pt-3">
                            <h4><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'</h4>
                            <ul class="mt-2 li-style">
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Unit Coordinator : '.$unit['uc_name'].'</li>
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Lecturer : '.$unit['lec_name'].'</li>
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Tutor : '.$unit['tutor_name'].'</li>
                            </ul>
                            <h4><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;Available Campus</h4>
                            <p>'.$unit['cam'].'</p>
                            <h4><i class="fas fa-bookmark"></i>&nbsp;&nbsp;Semester</h4>
                            <p>'.$unit['sem'].'</p>
    
                            <div class="card-footer text-muted text-center">
                                <h4><i class="fas fa-key"></i>&nbsp;&nbsp;  <button type="button" class="btn btn-light" onclick=enrol_in_unit('.$unit['id'].')>Enrol Now</button></h4>
                            </div>
                        </div>
                ';
            }
        }else{
            foreach ($units as $unit) {
                $res .= '
                        <div class="pl-0 pt-3">
                            <h4><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'</h4>
                            <ul class="mt-2 li-style">
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Unit Coordinator : '.$unit['uc_name'].'</li>
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Lecturer : '.$unit['lec_name'].'</li>
                                <li><i class="fas fa-user-tie"></i>&nbsp;&nbsp;Tutor : '.$unit['tutor_name'].'</li>
                            </ul>
                            <h4><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;Available Campus</h4>
                            <p>'.$unit['cam'].'</p>
                            <h4><i class="fas fa-bookmark"></i>&nbsp;&nbsp;Semester</h4>
                            <p>'.$unit['sem'].'</p>
    
                            <div class="card-footer text-muted text-center">
                                <h4><i class="fas fa-key"></i>&nbsp;&nbsp;  <button type="button" class="btn btn-light" onclick=unenrol_from_unit('.$unit['id'].')>Unenrol Now</button></h4>
                            </div>
                        </div>
                ';
            }
        }

       

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res, 'row'=>$row]);
        }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
        }
     }

     function load_unit_details(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['id']);

        $stmt = $conn->prepare("SELECT campus.campus AS cam,semesters.semester AS sem,ld.day_name AS lec_day,td.day_name AS tute_day,units.*,unit_data.* FROM units 
                                INNER JOIN campus ON campus.id = units.campus 
                                INNER JOIN semesters ON semesters.id = units.semester
                                INNER JOIN unit_data ON unit_data.unit_id = units.id
                                INNER JOIN days AS ld ON ld.id = unit_data.lecture_day
                                INNER JOIN days AS td ON td.id = unit_data.tutorial_day
                                WHERE units.id=?"
                              );
        $stmt->execute([$unit_id]); 

        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {
            $res .= '   <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Unit Name</label>
                            <div class="col-sm-10">
                                <input type="text" value="'.$unit['unit_name'].'" id="editUnitName" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Description</label>
                            <div class="col-sm-10">
                                <textarea class="form-control"  placeholder="'.$unit['description'].'" id="editUnitDesc" rows="5" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Campus</label>
                            <div class="col-sm-10">
                                <select class="browser-default custom-select mt-4 mb-4" id="editUnitCampus" required>
                                    <option selected disabled>'.$unit['cam'].'</option>
                                    <option value="1">Pandora</option>
                                    <option value="2">Rivendell</option>
                                    <option value="3">Neverland</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Semester</label>
                            <div class="col-sm-10">
                                <select class="browser-default custom-select" id="editUnitSem" required>
                                    <option selected disabled>'.$unit['sem'].'</option>
                                    <option value="1">Semester 01</option>
                                    <option value="2">Semester 02</option>
                                    <option value="3">Winter School</option>
                                    <option value="4">Spring School</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Lecture Day</label>
                            <div class="col-sm-10">
                                <select class="browser-default custom-select" id="editLecDay" required>
                                    <option selected disabled>'.$unit['lec_day'].'</option>
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Lecture Time</label>
                            <div class="col-sm-10">
                                <input type="time" id="editLecTime" value="'.$unit['lecture_time'].'" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Tutorial Day</label>
                            <div class="col-sm-10">
                                <select class="browser-default custom-select" id="editTuteDay" required>
                                    <option selected disabled>'.$unit['tute_day'].'</option>
                                    <option value="1">Monday</option>
                                    <option value="2">Tuesday</option>
                                    <option value="3">Wednesday</option>
                                    <option value="4">Thursday</option>
                                    <option value="5">Friday</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Tutorial Time</label>
                            <div class="col-sm-10">
                                <input type="time" id="editTuteTime" value="'.$unit['tutorial_time'].'" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Location</label>
                            <div class="col-sm-10">
                                <input type="text" id="editLocation" value="'.$unit['location'].'" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="hidden" id="editUnitId" value="'.$unit_id.'"/>
                                <button type="submit" class="btn btn-default btn-sm">Save changes</button>
                            </div>
                        </div>';
        }

         if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
          }else{
             echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
          }

     }

     function list_tutors(){
        $conn = db_conn();
        $stmt_ut = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_ut->execute([4]); 

        $unit_tutor = $stmt_ut->fetchAll();
        $unit_tutor_res = '';
        $unit_tutor_res = '<select class="browser-default custom-select" id="editUnitTutor" required><option selected disabled>Select Unit Tutor</option>';
        foreach ($unit_tutor as $ut) {
            $unit_tutor_res .= '  
                        <option value="'.$ut['id'].'">'.$ut['name'].'</option>
            ';
        }
        $unit_tutor_res .= '</select>';

        if($unit_tutor_res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','unit_tutor_res'=>$unit_tutor_res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function list_lecturers(){
        $conn = db_conn();

        $stmt_lec = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_lec->execute([3]); 

        $unit_lecturer = $stmt_lec->fetchAll();
        $unit_lecturer_res = '';
        $unit_lecturer_res = '<select class="browser-default custom-select" id="editUnitLecturer" required><option selected disabled>Select Unit Lecturer</option>';
        foreach ($unit_lecturer as $ul) {
            $unit_lecturer_res .= '  
                        <option value="'.$ul['id'].'">'.$ul['name'].'</option>
            ';
        }
        $unit_lecturer_res .= '</select>';

        if($unit_lecturer_res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','unit_lecturer_res'=>$unit_lecturer_res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function list_coordinators(){
        $conn = db_conn();

        $stmt_uc = $conn->prepare("SELECT * FROM staff 
                                INNER JOIN staff_positions ON staff_positions.id = staff.position
                                WHERE staff.position=?"
                              );
        $stmt_uc->execute([2]); 

        $unit_coordinator = $stmt_uc->fetchAll();
        $unit_coordinator_res = '';
        $unit_coordinator_res = '<select class="browser-default custom-select" id="editUnitCo" required><option selected disabled>Select Unit Coordinator</option>';
        foreach ($unit_coordinator as $uc) {
            $unit_coordinator_res .= '  
                        <option value="'.$uc['id'].'">'.$uc['name'].'</option>
            ';
        }
        $unit_coordinator_res .= '</select>';

        if($unit_coordinator_res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','unit_coordinator_res'=>$unit_coordinator_res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function edit_unit(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['id']);
        $unitName = trim($data['unitName']);
        $unitDescription = trim(filter_var($data['unitDesc'],FILTER_SANITIZE_STRING));
        $unitCampus = trim($data['unitCampus']);
        $unitSem = trim($data['unitSem']);

        $unitLecDay = trim($data['unitLecDay']);
        $unitLecTime = trim($data['unitLecTime']);
        $unitTuteDay = trim($data['unitTuteDay']);
        $unitTuteTime = trim($data['unitTuteTime']);
        $unitLocation = trim($data['unitLocation']);

        $userId = $_SESSION["user_id"];

        $stmt = $conn->prepare("UPDATE units SET unit_name=?, description=?, semester=?, campus=?, created_by=? WHERE id=?");
        $units_update = $stmt->execute([$unitName,$unitDescription,$unitSem,$unitCampus,$userId,$unit_id]);

        $stmt_unit_data = $conn->prepare("UPDATE unit_data SET created_by=?, lecture_day=?, lecture_time=?, tutorial_day=?, tutorial_time=?, location=? WHERE unit_id=?");
        $unit_data_update = $stmt_unit_data->execute([$userId,$unitLecDay,$unitLecTime,$unitTuteDay,$unitTuteTime,$unitLocation,$unit_id]);

        if($units_update && $unit_data_update){
            echo json_encode(['status_code'=>200,'message'=>'Success !','units_update'=>$units_update,'unit_data_update'=>$unit_data_update]);
        }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
        }
     }

     function remove_unit(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['id']);

        $stmt = $conn->prepare("DELETE FROM units WHERE id=?");

        $stmt_unit_data = $conn->prepare("DELETE FROM unit_data WHERE unit_id=?");

        if($stmt->execute([$unit_id]) && $stmt_unit_data->execute([$unit_id])){
            echo json_encode(['status_code'=>200,'message'=>'Success !']);
        }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
        }

     }

     function get_all_units_for_enrolled_students(){
        $conn = db_conn();
        $stmt = $conn->prepare("SELECT id,unit_name FROM units");
        $stmt->execute(); 
        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {
            $res .= '<li class="list-group-item"><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'&nbsp;&nbsp;
                        <a class="btn-floating float-right btn-sm btn-info mr-2" onclick=enrolled_students_list('.$unit['id'].')><i class="fas fa-eye text-white"></i></a>
                    </li>';
        }

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }

     function enrolled_students_list(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['id']);

        $stmt = $conn->prepare("SELECT students.name, students.student_id, students.email,DATE(student_have_units.created_at) AS enrolled_date
                                FROM student_have_units
                                INNER JOIN units ON units.id = student_have_units.unit_id
                                INNER JOIN students ON students.id = student_have_units.student_id
                                WHERE units.id = ?"
                              );
        $stmt->execute([$unit_id]); 

        $students = $stmt->fetchAll();
        $res = '';
        foreach ($students as $student) {
            $res .= '<tr>
                        <td>'.$student['name'].'</td>
                        <td>'.$student['student_id'].'</td>
                        <td>'.$student['email'].'</td>
                        <td>'.$student['enrolled_date'].'</td>
                    </tr>
                    ';
        }

        if($res != ''){
            echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
         }else{
            echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
         }
     }
}
