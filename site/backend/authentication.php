<?php
class Authentication
{
    function student_registration()
    {
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $student_id = 'stu-' . trim($data['student_id']);
        $name = $data['name'];
        $email = trim($data['email']);
        $password = trim($data['password']);
        $dob = trim($data['dob']);
        $phone = trim($data['phone']);
        $address = $data['address'];

        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "INSERT INTO students (student_id,role_id,name,email,address,dob,phone,password) VALUES 
           ('$student_id', 1, '$name', '$email', '$address', '$dob', '$phone', '$password')";

        $res = $conn->exec($sql);

        if ($res > 0) {
            $last_id = $conn->lastInsertId();
            $_SESSION["user_id"] = $last_id;
            $_SESSION["name"] = $name;
            echo $res;
        }
    }

    function staff_registration()
    {
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $staff_id = 'sta-' . trim($data['staff_id']);
        $name = $data['name'];
        $email = trim($data['email']);
        $password = trim($data['password']);
        $expertise = trim($data['expertise']);
        $qualifications = trim($data['qualifications']);
        $phone = trim($data['phone']);
        $address = $data['address'];

        $password = password_hash($password, PASSWORD_DEFAULT);

        $sql = "INSERT INTO staff (staff_id,role_id,name,email,address,phone,password,expertise,qualifications) VALUES 
           ('$staff_id', 2, '$name', '$email', '$address', '$phone', '$password', '$expertise', '$qualifications')";

        $res = $conn->exec($sql);

        if ($res > 0) {
            $last_id = $conn->lastInsertId();
            $_SESSION["user_id"] = $last_id;
            $_SESSION["name"] = $name;
            echo json_encode(['status' => 200, 'message' => 'Success !']);
        }
    }

    function login()
    {
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $type = trim($data['type']);
        $email = trim($data['email']);
        $password = trim($data['password']);

        if ($type == 'student') {
            $stmt = $conn->prepare("SELECT * FROM students WHERE email=?");
        } else {
            $stmt = $conn->prepare("SELECT * FROM staff WHERE email=?");
        }

        $stmt->execute([$email]);

        $user = $stmt->fetch();

        if ($user) {
            if (password_verify($password, $user['password'])) {
                $_SESSION["user_id"] = $user['id'];
                $_SESSION["name"] = $user['name'];
                $_SESSION["user_type"] = $type;
                echo json_encode(['status_code' => 200, 'message' => 'Success !']);
            } else {
                echo json_encode(['status_code' => 401, 'message' => 'Email or password is incorrect !']);
            }
        } else {
            echo json_encode(['status_code' => 401, 'message' => 'Account does not exist !']);
        }
    }

    function logout()
    {
        try {
            // remove all session variables
            session_unset();
            // destroy the session
            session_destroy();
            echo json_encode(['status_code' => 200, 'message' => 'Success !']);
        } catch (\Exception  $e) {
            echo json_encode(['status_code' => 500, 'message' => $e->getMessage()]);
        }
    }
}
