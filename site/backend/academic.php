<?php
class Academic
{

    function load_academic_staff()
    {
        $conn = db_conn();

        $stmt = $conn->prepare("SELECT * FROM staff");

        $stmt->execute();

        $staff = $stmt->fetchAll();

        $staff_res = '';

        foreach ($staff as $st) {
            $staff_res .= '  
                <div class="card">
                    <div class="card-header p-0 z-depth-1" style="background-color: #ffffff;" role="tab"
                        id="heading' . $st['id'] . '">
                        <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse' . $st['id'] . '"
                            aria-expanded="true" aria-controls="collapse' . $st['id'] . '">
                            <img src="assets/images/avatar.jpg" class="mr-4 float-left" alt="avatar"
                                width="50" height="60">
                            <h4 class="black-text mb-0 py-3 mt-1">
                                ' . $st['name'] . '
                            </h4>
                        </a>
                        
                    </div>

                    <div id="collapse' . $st['id'] . '" class="collapse show" role="tabpanel" aria-labelledby="heading' . $st['id'] . '"
                        data-parent="#accordionEx5">
                        <div class="card-body rgba-black-light white-text z-depth-1">
                            <div class="d-flex flex-column p-md-4 mb-0">
                                <p>Qualifications : ' . $st['qualifications'] . '</p>
                                <p>Expertise : ' . $st['expertise'] . '</p>
                                <p>Prefered Days : Monday - Friday</p>
                                <p>Consultation Hours : 8 am - 5 pm</p>
                                <button type="button" class="btn btn-danger" onclick=remove_staff(' . $st['id'] . ')><i class="fas fa-trash-alt text-white"></i>&nbsp;&nbsp;Remove Staff</button>
                            </div>
                        </div>
                    </div>
                </div>
                ';
        }

        if ($staff_res != '') {
            echo json_encode(['status_code' => 200, 'message' => 'Success !', 'staff_res' => $staff_res]);
        } else {
            echo json_encode(['status_code' => 500, 'message' => 'Oops, An error occured.. Try again later !']);
        }
    }

    function get_all_units_for_academic()
    {
        $conn = db_conn();
        $stmt = $conn->prepare("SELECT id,unit_name FROM units");
        $stmt->execute();
        $units = $stmt->fetchAll();
        $res = '';
        foreach ($units as $unit) {

            $res .= '<li class="list-group-item"><i class="fas fa-book"></i>&nbsp;&nbsp;' . $unit['unit_name'] . '&nbsp;&nbsp;
                            <a class="btn-floating float-right btn-sm btn-default" data-toggle="modal"
                                data-target="#centralModalSm" onclick=model_data(' . $unit['id'] . ')><i class="fas fa-plus text-white"></i></a>
                        </li>';
        }

        if ($res != '') {
            echo json_encode(['status_code' => 200, 'message' => 'Success !', 'results' => $res]);
        } else {
            echo json_encode(['status_code' => 500, 'message' => 'Oops, An error occured.. Try again later !']);
        }
    }

    function update_unit_staff_from_academic()
    {
        $conn = db_conn();

        $data = json_decode(file_get_contents("php://input"), true);

        $unit_id = trim($data['selected_unit_id']);
        $unit_co = trim($data['unit_co']);
        $unit_lec = trim($data['unit_lec']);
        $unit_tutor = trim($data['unit_tutor']);
        $userId = $_SESSION["user_id"];

        $sql = "UPDATE unit_data SET lecturer_id=?, unit_coordinator_id=?, tutor_id=?, created_by=? WHERE unit_id=?";

        if ($conn->prepare($sql)->execute([$unit_lec, $unit_co, $unit_tutor, $userId, $unit_id])) {
            echo json_encode(['status_code' => 200, 'message' => 'Success !']);
        } else {
            echo json_encode(['status_code' => 500, 'message' => 'Oops, An error occured.. Try again later !']);
        }
    }

    function remove_staff()
    {
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $staff_id = trim($data['id']);

        $stmt = $conn->prepare("DELETE FROM staff WHERE id=?");

        if ($stmt->execute([$staff_id])) {
            echo json_encode(['status_code' => 200, 'message' => 'Success !']);
        } else {
            echo json_encode(['status_code' => 500, 'message' => 'Oops, An error occured.. Try again later !']);
        }
    }

    function enrol_in_unit(){
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);
        $userId = $_SESSION["user_id"];
        $unit_id = trim($data['unit_id']);

        $stmt = $conn->prepare('SELECT count(*) as row_count FROM student_have_units WHERE student_id=? AND unit_id=?');
        $stmt->execute([$userId,$unit_id]); 
        $row = $stmt->fetch();

        if($row['row_count'] == '0'){
            $sql = "INSERT INTO student_have_units (student_id,unit_id) VALUES 
            ('$userId', '$unit_id')";
            $res = $conn->exec($sql);
            if($res > 0){
                echo json_encode(['status_code'=>200,'message'=>'Success !']);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }
        }else{
            echo json_encode(['status_code'=>500,'message'=>'You are already enrolled in this course !']);
        }
      
    }

    function unenrol_from_unit()
    {
        $conn = db_conn();
        $data = json_decode(file_get_contents("php://input"), true);

        $userId = $_SESSION["user_id"];
        $unit_id = trim($data['unit_id']);

        $stmt = $conn->prepare("DELETE FROM student_have_units WHERE student_id=? AND unit_id=?");

        if ($stmt->execute([$userId],$unit_id)) {
            echo json_encode(['status_code' => 200, 'message' => 'Success !']);
        } else {
            echo json_encode(['status_code' => 500, 'message' => 'Oops, An error occured.. Try again later !']);
        }
    }
}
