<?php

    session_start();
    require 'dbcon.php';
    include 'authentication.php';
    include 'unit-management.php';
    include 'academic.php';
    include 'tutorial-allocation.php';
    include 'timetable.php';
    include 'my-account.php';

    $authentication = new Authentication();
    $unitManager = new UnitManagement();
    $academic = new Academic();
    $tuteAllocation = new TutorialAllocation();
    $timeTable = new TimeTable();
    $myAccount = new MyAccount();

    switch($_GET['f']) {
        case 'student_registration':
            $authentication->student_registration();
            break;
        case 'staff_registration':
            $authentication->staff_registration();
            break;
        case 'login':
            $authentication->login();
            break;
        case 'logout':
            $authentication->logout();
            break;
        case 'add_new_unit':
            $unitManager->add_new_unit();
            break;
        case 'get_all_units':
            $unitManager->get_all_units();
            break;
        case 'load_unit_details':
            $unitManager->load_unit_details();
            break;
        case 'edit_unit':
            $unitManager->edit_unit();
            break;
        case 'remove_unit':
            $unitManager->remove_unit();
            break;
        case 'list_units':
            $unitManager->list_units();
            break;
        case 'list_unit_details':
            $unitManager->list_unit_details();
            break;
        case 'ue_list_unit_details':
            $unitManager->ue_list_unit_details();
            break;
        case 'load_tutors_uc_lec':
            $unitManager->load_tutors_uc_lec();
            break;
        case 'list_coordinators':
            $unitManager->list_coordinators();
            break;
        case 'list_lecturers':
            $unitManager->list_lecturers();
            break;
        case 'list_tutors':
            $unitManager->list_tutors();
            break;
        case 'get_all_units_for_enrolled_students':
            $unitManager->get_all_units_for_enrolled_students();
            break;
        case 'enrolled_students_list':
            $unitManager->enrolled_students_list();
            break;
        case 'load_academic_staff':
            $academic->load_academic_staff();
            break;
        case 'get_all_units_for_academic':
            $academic->get_all_units_for_academic();
            break;
        case 'update_unit_staff_from_academic':
            $academic->update_unit_staff_from_academic();
            break;
        case 'remove_staff':
            $academic->remove_staff();
            break;
        case 'enrol_in_unit':
            $academic->enrol_in_unit();
            break;
        case 'unenrol_from_unit':
            $academic->unenrol_from_unit();
            break;
        case 'list_enrolled_units':
            $tuteAllocation->list_enrolled_units();
            break;
        case 'load_unit_times':
            $tuteAllocation->load_unit_times();
            break;
        case 'confirm_unit_times':
            $tuteAllocation->confirm_unit_times();
            break;   
        case 'load_time_table':
            $timeTable->load_time_table();
            break;     
        case 'load_account_data':
            $myAccount->load_account_data();
            break;    
        case 'edit_staff_profile':
            $myAccount->edit_staff_profile();
            break;
        case 'edit_student_profile':
            $myAccount->edit_student_profile();
            break;
            
            
            
            
            
            
        
            
    }
