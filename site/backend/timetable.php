<?php
    class TimeTable {

        function load_time_table(){
            $conn = db_conn();
            $userId = $_SESSION["user_id"];
            $stmt = $conn->prepare("SELECT 	units.unit_name,
                                            unit_data.id AS udata_id,
                                            unit_data.lecture_day AS lec_day_no, 
                                            ld.day_name AS lec_day_name,
                                            unit_data.lecture_time AS lec_time,
                                            unit_data.tutorial_day AS tute_day_no, 
                                            td.day_name AS tute_day_name,
                                            unit_data.tutorial_time AS tute_time,
                                            unit_data.location
                                    FROM
                                    units
                                    INNER JOIN unit_data ON unit_data.unit_id = units.id
                                    LEFT JOIN student_have_units ON student_have_units.unit_data_id = unit_data.id
                                    INNER JOIN days AS ld ON ld.id = unit_data.lecture_day
                                    INNER JOIN days AS td ON td.id = unit_data.tutorial_day
                                    WHERE student_have_units.student_id = ?
                                    AND student_have_units.unit_data_id <> ''
                                  ");

            $stmt->execute([$userId]); 

            $times = $stmt->fetchAll();

            $res = '';

            foreach ($times as $time) {
                $res .= '<tr>
                            <td>'.$time['unit_name'].'</td>
                            <td>'.$time['lec_day_name'].'</td>
                            <td>'.$time['lec_time'].'</td>
                            <td>'.$time['tute_day_name'].'</td>
                            <td>'.$time['tute_time'].'</td>
                            <td>'.$time['location'].'</td>
                        </tr>';
            }

            if($res != ''){
                echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'You don\'t have any enrolled units !']);
            }
            
        }
    }


?>