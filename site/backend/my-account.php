<?php
    class MyAccount {

        function load_account_data(){
            $conn = db_conn();
            $userId = $_SESSION["user_id"];
            $userType = $_SESSION["user_type"];
            $res = '';

            if($userType == 'student'){
                $stmt = $conn->prepare("SELECT * FROM students WHERE id=?");
                $stmt->execute([$userId]);
                $students = $stmt->fetchAll();

                foreach ($students as $student) {
                    $res .= '<div class="form-group row mt-4">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$student['name'].'" id="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$student['email'].'" id="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$student['address'].'" id="address" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Date of Birth</label>
                                <div class="col-sm-10">
                                    <input type="date" value="'.$student['dob'].'" id="dob" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$student['phone'].'" id="phone" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">New Password</label>
                                <div class="col-sm-10">
                                    <input type="password" id="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input type="password" id="cpassword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="hidden" id="user_type" value="student" />
                                    <button type="submit" class="btn btn-default btn-sm">Save changes</button>
                                </div>
                            </div>';
                }

            }else{
                $stmt = $conn->prepare("SELECT * FROM staff WHERE id=?");
                $stmt->execute([$userId]);
                $staff = $stmt->fetchAll();

                foreach ($staff as $s) {
                    $res .= '<div class="form-group row mt-4">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['name'].'" id="name" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['email'].'" id="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Address</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['address'].'" id="address" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['phone'].'" id="phone" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Qualifications</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['qualifications'].'" id="qualifications" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Expertise</label>
                                <div class="col-sm-10">
                                    <input type="text" value="'.$s['expertise'].'" id="expertise" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">New Password</label>
                                <div class="col-sm-10">
                                    <input type="password" id="password" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Confirm Password</label>
                                <div class="col-sm-10">
                                    <input type="password" id="cpassword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="hidden" id="user_type" value="staff" />
                                    <button type="submit" class="btn btn-default btn-sm">Save changes</button>
                                </div>
                            </div>';
                }
            }

            if($res != ''){
                echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }
            
        }

        function edit_student_profile(){
            $conn = db_conn();
            $data = json_decode(file_get_contents("php://input"), true);

            $userId = $_SESSION["user_id"];
            $userType = $_SESSION["user_type"];

            $name = trim($data['name']);
            $email = trim($data['email']);
            $address = trim($data['address']);
            $phone = trim($data['phone']);
            $dob = trim($data['dob']);
            $password = trim($data['password']);

            $profile_update = '';

            if($password != ''){
                $password = password_hash($password, PASSWORD_DEFAULT);
                $stmt = $conn->prepare("UPDATE students SET name=?, email=?, address=?, phone=?, dob=?, password=? WHERE id=?");
                $profile_update = $stmt->execute([$name,$email,$address,$phone,$dob,$password,$userId]);
            }else{
                $stmt = $conn->prepare("UPDATE students SET name=?, email=?, address=?, phone=?, dob=? WHERE id=?");
                $profile_update = $stmt->execute([$name,$email,$address,$phone,$dob,$userId]);
            }

            if($profile_update){
                echo json_encode(['status_code'=>200,'message'=>'Success !']);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }
 
        }

        function edit_staff_profile(){
            $conn = db_conn();
            $data = json_decode(file_get_contents("php://input"), true);

            $userId = $_SESSION["user_id"];
            $userType = $_SESSION["user_type"];

            $name = trim($data['name']);
            $email = trim($data['email']);
            $address = trim($data['address']);
            $phone = trim($data['phone']);
            $qualifications = trim($data['qualifications']);
            $expertise = trim($data['expertise']);
            $password = trim($data['password']);

            $profile_update = '';

            if($password != ''){
                $password = password_hash($password, PASSWORD_DEFAULT);
                $stmt = $conn->prepare("UPDATE staff SET name=?, email=?, address=?, phone=?, expertise=?, qualifications=?, password=? WHERE id=?");
                $profile_update = $stmt->execute([$name,$email,$address,$phone,$expertise,$qualifications,$password,$userId]);
            }else{
                $stmt = $conn->prepare("UPDATE staff SET name=?, email=?, address=?, phone=?, expertise=?, qualifications=? WHERE id=?");
                $profile_update = $stmt->execute([$name,$email,$address,$phone,$expertise,$qualifications,$userId]);
            }

            if($profile_update){
                echo json_encode(['status_code'=>200,'message'=>'Success !']);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }
        }
    }
