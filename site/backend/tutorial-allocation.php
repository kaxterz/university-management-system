<?php 
    class TutorialAllocation {

        function list_enrolled_units(){
            $conn = db_conn();
            $userId = $_SESSION["user_id"];
            $stmt = $conn->prepare("SELECT units.*
                                    FROM units
                                    INNER JOIN student_have_units ON student_have_units.unit_id = units.id
                                    WHERE student_have_units.student_id = ?
                                  ");
            $stmt->execute([$userId]); 

            $units = $stmt->fetchAll();
            $res = '';
            foreach ($units as $unit) {
                $res .= '<li class="list-group-item"><i class="fas fa-book"></i>&nbsp;&nbsp;'.$unit['unit_name'].'&nbsp;&nbsp;
                            <a class="btn-floating float-right btn-sm btn-info mr-2" onclick=load_unit_times('.$unit['id'].')><i class="fas fa-eye text-white"></i></a>
                        </li>';
            }

            if($res != ''){
                echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'You don\'t have any enrolled units !']);
            }
        }

        function load_unit_times(){
            $conn = db_conn();
            $data = json_decode(file_get_contents("php://input"), true);
            $unit_id = trim($data['id']);

            $stmt = $conn->prepare("SELECT  unit_data.id as udata_id,
                                            ld.day_name AS lec_day,
                                            unit_data.lecture_time,
                                            td.day_name AS tute_day,
                                            unit_data.tutorial_time,
                                            unit_data.location
                                    FROM
                                    unit_data 
                                    INNER JOIN days AS ld ON ld.id = unit_data.lecture_day
                                    INNER JOIN days AS td ON td.id = unit_data.tutorial_day
                                    WHERE unit_data.unit_id = ?
                                  ");

            $stmt->execute([$unit_id]); 
            
            $units = $stmt->fetchAll();
            $res = '<div class="text-center mt-4">
                        <table class="table table-striped">
                        <tbody>';
            foreach ($units as $unit) {
                $res .= '<tr>
                            <th>Lecture Day</th>
                            <td>'.$unit['lec_day'].'</td>
                        </tr>
                        <tr>
                            <th>Lecture Time</th>
                            <td>'.$unit['lecture_time'].'</td>
                        </tr>
                        <tr>
                            <th>Tutorial Day</th>
                            <td>'.$unit['tute_day'].'</td>
                        </tr>
                        <tr>
                            <th>Tutorial Time</th>
                            <td>'.$unit['tutorial_time'].'</td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td>'.$unit['location'].'</td>
                        </tr>';
            }

            $res .= '   </tbody>
                        </table>
                        <p><small>By confirming you agree to attend the lectures and tutorials on above days and times.</small></p>
                        <button type="button" class="btn btn-default" onclick=confirm_unit_times('.$unit_id.','.$unit['udata_id'].')>Confirm</button>
                    </div>';

            if($res != ''){
                echo json_encode(['status_code'=>200,'message'=>'Success !','results'=>$res]);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }

        }

        function confirm_unit_times(){
            $conn = db_conn();
            $userId = $_SESSION["user_id"];
            $data = json_decode(file_get_contents("php://input"), true);
            $unit_id = trim($data['id']);
            $unit_data_id = trim($data['unit_data_id']);

            $stmt = $conn->prepare("UPDATE student_have_units SET unit_data_id=? WHERE student_id=? AND unit_id=?");
            $update = $stmt->execute([$unit_data_id,$userId,$unit_id]);

            if($update){
                echo json_encode(['status_code'=>200,'message'=>'Success !']);
            }else{
                echo json_encode(['status_code'=>500,'message'=>'Oops, An error occured.. Try again later !']);
            }
        }
    }
