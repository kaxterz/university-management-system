/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 10.4.11-MariaDB : Database - uodw
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uodw` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `uodw`;

/*Table structure for table `campus` */

DROP TABLE IF EXISTS `campus`;

CREATE TABLE `campus` (
  `id` tinyint(5) NOT NULL AUTO_INCREMENT,
  `campus` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `campus` */

insert  into `campus`(`id`,`campus`,`created_at`) values 
(1,'Pandora','2020-05-25 13:49:48'),
(2,'Rivendell','2020-05-25 13:49:41'),
(3,'Neverland','2020-05-25 13:49:50');

/*Table structure for table `days` */

DROP TABLE IF EXISTS `days`;

CREATE TABLE `days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day_name` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `days` */

insert  into `days`(`id`,`day_name`,`created_at`) values 
(1,'Monday','2020-05-26 10:53:17'),
(2,'Tuesday','2020-05-26 10:53:21'),
(3,'Wednesday','2020-05-26 10:53:26'),
(4,'Thursday','2020-05-26 10:53:30'),
(5,'Friday','2020-05-26 10:53:34'),
(6,'Saturday','2020-05-26 10:53:39'),
(7,'Sunday','2020-05-26 10:53:43');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` tinyint(5) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `roles` */

/*Table structure for table `semesters` */

DROP TABLE IF EXISTS `semesters`;

CREATE TABLE `semesters` (
  `id` tinyint(5) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `semesters` */

insert  into `semesters`(`id`,`semester`,`created_at`) values 
(1,'Semester 01','2020-05-25 13:50:07'),
(2,'Semester 02','2020-05-25 13:50:18'),
(3,'Winter School','2020-05-25 13:50:24'),
(4,'Spring School','2020-05-25 13:50:26');

/*Table structure for table `staff` */

DROP TABLE IF EXISTS `staff`;

CREATE TABLE `staff` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `staff_id` varchar(255) NOT NULL,
  `role_id` tinyint(5) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `qualifications` varchar(255) NOT NULL,
  `expertise` varchar(255) NOT NULL,
  `phone` int(10) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(255) NOT NULL,
  `position` tinyint(5) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `staff` */

insert  into `staff`(`id`,`staff_id`,`role_id`,`email`,`address`,`qualifications`,`expertise`,`phone`,`password`,`created_at`,`name`,`position`,`gender`) values 
(1,'sta-01',2,'j.smith@gmail.com','Australia','PHD, Master','',0,'$2y$10$Ay5DsPZI0gwg3lZFAtnZFOnj8eGKPVkLpNRosnEB/oGuK9exLgnNW','2020-05-25 18:18:18','John',2,NULL),
(2,'sta-02',2,'mack@gmail.com','Australia','Master','NAA',1597536480,'$2y$10$m9f.wGzSZH6kXwjGFFMg1ugUvropMgvpcylBhZ2O8jYBhz0VSPREK','2020-05-27 01:24:26','Mack',3,NULL),
(4,'sta-04',2,'neel@gmail.com','Australia','PHD, Master','Information Systems',258963471,'$2y$10$RhnBuW9Br60t5fAulyc8zu46fKuD4jGUIbpRI1KDTxRl7qN2v9s/C','2020-05-26 11:12:54','Neel',NULL,NULL);

/*Table structure for table `staff_positions` */

DROP TABLE IF EXISTS `staff_positions`;

CREATE TABLE `staff_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `staff_positions` */

insert  into `staff_positions`(`id`,`position`,`created_at`) values 
(1,'Degree Coordinator','2020-05-25 18:01:38'),
(2,'Unit Coordinator','2020-05-25 18:01:48'),
(3,'Lecturer','2020-05-25 18:01:52'),
(4,'Tutor','2020-05-25 18:02:05');

/*Table structure for table `student_have_units` */

DROP TABLE IF EXISTS `student_have_units`;

CREATE TABLE `student_have_units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` tinyint(5) NOT NULL,
  `unit_id` tinyint(5) NOT NULL,
  `unit_data_id` tinyint(5) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

/*Data for the table `student_have_units` */

insert  into `student_have_units`(`id`,`student_id`,`unit_id`,`unit_data_id`,`created_at`) values 
(2,4,12,7,'2020-05-26 13:56:21'),
(3,4,10,5,'2020-05-26 15:51:59');

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(255) NOT NULL,
  `role_id` tinyint(5) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `password` varchar(255) NOT NULL,
  `gender` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `students` */

insert  into `students`(`id`,`student_id`,`role_id`,`name`,`email`,`address`,`dob`,`phone`,`created_at`,`password`,`gender`) values 
(1,'stu-01',1,'John','john@gmail.com','Australia','2020-05-23',1597538240,'2020-05-23 12:14:46','9TCDOUa8ccFHo',NULL),
(2,'stu-02',1,'Bruce','bruce@gmail.com','Australia','2020-05-22',1112225,'2020-05-23 12:33:37','$2y$10$8z0q9V2qi3Azkxix.0k98O7QZAK4DiCsoplBdvG.4fEmoSYOLELIm',NULL),
(4,'stu-03',1,'Will','will@gmail.com','Australia','2020-05-22',2147483647,'2020-05-27 01:01:40','$2y$10$upk1bvoN36FH7to6ZvjzT.UWWtytb0aJdyaymFk5bcdDTjJ6i5Mvy',NULL),
(7,'stu-05',1,'Mali','mali@gmail.com','Australia','2020-05-29',2147483647,'2020-05-24 15:47:20','$2y$10$u1y.tbzCpyHH0yyUgYSa/uUOERR8v5HbrrH/q//L1djSFmrGBEpOG',NULL);

/*Table structure for table `unit_data` */

DROP TABLE IF EXISTS `unit_data`;

CREATE TABLE `unit_data` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `unit_id` tinyint(5) NOT NULL,
  `lecturer_id` tinyint(5) NOT NULL,
  `unit_coordinator_id` tinyint(5) NOT NULL,
  `tutor_id` tinyint(5) NOT NULL,
  `created_by` tinyint(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `lecture_day` tinyint(5) DEFAULT NULL,
  `lecture_time` varchar(255) DEFAULT NULL,
  `tutorial_day` tinyint(5) DEFAULT NULL,
  `tutorial_time` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

/*Data for the table `unit_data` */

insert  into `unit_data`(`id`,`unit_id`,`lecturer_id`,`unit_coordinator_id`,`tutor_id`,`created_by`,`created_at`,`lecture_day`,`lecture_time`,`tutorial_day`,`tutorial_time`,`location`) values 
(5,10,3,2,0,4,'2020-05-26 15:57:31',3,'13:30',5,'15:30','Hall 01'),
(6,11,3,2,0,4,'2020-05-26 11:53:11',2,'13:30',3,'15:30','Hall 02'),
(7,12,3,2,0,4,'2020-05-26 12:14:04',2,'13:00',2,'15:00','Hall 04');

/*Table structure for table `units` */

DROP TABLE IF EXISTS `units`;

CREATE TABLE `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `semester` varchar(255) NOT NULL,
  `campus` varchar(255) NOT NULL,
  `created_by` tinyint(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

/*Data for the table `units` */

insert  into `units`(`id`,`unit_name`,`description`,`semester`,`campus`,`created_by`,`created_at`) values 
(10,'Unit 01','shows how you could write the multiplication table program with a for loop.','','',4,'2020-05-26 15:53:34'),
(11,'Unit 02','W3Schools is optimized for learning, testing, and training. Examples might be simplified to improve reading and basic understanding.','2','4',4,'2020-05-26 11:53:11'),
(12,'Unit 03','An impressive collection of stunning Material Design components. Whenever you see MDB PRO COMPONENT button know that the component you are looking at is developed by MDB professionals and officially supported in MDB.','3','2',4,'2020-05-26 12:14:03');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
